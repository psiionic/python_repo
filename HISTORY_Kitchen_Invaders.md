[5.3.18] Ver. 1 - I was able to create a player, the bullets and the enemies. The enemies are placed at random, once they are shot, 
they disappear and reappear at another spot. Once, the player is touched by any of the enemies, it's game over.
[5.10.18] Ver. 1.1- I was able to make the roaches move left to right. Once shot, they regenerate at a randomized position and move 
in sync with the other roaches.
[5.17.18] Ver. 2 - I placed images onto the player and enemies. The player is now a burger and the enemies are roaches.
[5.24.18] Ver. 2.1 - I played around with different backgrounds and settled for the current one.
[5.31.18] Ver. 2.3 - I added a scoring function which counts how many points the player has.